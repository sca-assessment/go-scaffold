module scaffoldproject

go 1.18

// ADD YOUR DEPENDENCIES HERE
require (
	github.com/opencontainers/runc v1.0.0-rc6 // Known vulnerable package
	github.com/sirupsen/logrus v1.8.1 // Known good package
)