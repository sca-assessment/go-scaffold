# Use the official Golang image to build the application
FROM golang:1.18 as builder

# Set the working directory
WORKDIR /app

# Copy the Go modules manifests
COPY go.mod .
COPY go.sum .

# Download the Go module dependencies
RUN go mod download

# Copy the source code
COPY . .

# Build the application
RUN go build -o scaffoldproject

# Use a minimal image to run the application
FROM debian:buster-slim

# Set the working directory
WORKDIR /app

# Copy the built binary from the builder stage
COPY --from=builder /app/scaffoldproject .
# Copy the go.mod and go.sum files to install dependencies for Snyk test
COPY --from=builder /app/go.mod .
COPY --from=builder /app/go.sum .
COPY --from=builder /go/pkg /go/pkg

# Install additional dependencies if needed (e.g., sh for the vulnerable function)
RUN apt-get update && apt-get install -y \
    procps \
    curl \
    && rm -rf /var/lib/apt

