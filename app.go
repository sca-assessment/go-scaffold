package main

import (
    "fmt"
    "os"
    "os/exec"
    
    // Import your dependencies here
	"github.com/sirupsen/logrus" // Known Good
	"github.com/opencontainers/runc/libcontainer" // Known Bad
	"github.com/opencontainers/runc/libcontainer/configs"
)

func main() {
	// Use your dependencies in code or Go will complain
	// Safe usage of logrus
	log := logrus.New()
	log.Info("Starting application...")

	// Vulnerable usage of runc library
	executeVulnerableCommand()

	log.Info("Application finished.")
}

// executeVulnerableCommand demonstrates a vulnerable function
func executeVulnerableCommand() {
	// This is a simplified example to demonstrate the vulnerability
	factory, err := libcontainer.New("/tmp/container", libcontainer.Cgroupfs)
	if err != nil {
		fmt.Println("Error creating container factory:", err)
		return
	}

	config := &configs.Config{
		Rootfs: "/tmp/rootfs",
		// Vulnerable configuration: allows command injection
		Capabilities: &configs.Capabilities{
			Bounding: []string{"CAP_SYS_ADMIN"},
		},
	}

	container, err := factory.Create("demo", config)
	if err != nil {
		fmt.Println("Error creating container:", err)
		return
	}

	process := &libcontainer.Process{
		Args: []string{"sh", "-c", "echo vulnerable > /proc/self/fd/1"},
		Env:  []string{"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"},
		Cwd:  "/",
	}

	err = container.Run(process)
	if err != nil {
		fmt.Println("Error running process:", err)
		return
	}

	// Wait for the process to finish
	status, err := process.Wait()
	if err != nil {
		fmt.Println("Error waiting for process:", err)
		return
	}

	fmt.Println("Process exited with status:", status)
}

func checkVulnerability() {
	// This function demonstrates how an attacker might exploit the vulnerability
	// by replacing the runc binary with a malicious one.
	fmt.Println("Checking vulnerability...")

	// Command injection to overwrite runc binary
	cmd := exec.Command("/bin/sh", "-c", "echo hacked > /proc/self/exe")
	err := cmd.Run()
	if err != nil {
		fmt.Println("Command injection failed:", err)
		return
	}

	fmt.Println("Command injection successful.")
}